export const stylesheet = (
  href: string,
  options?: { integrity: string; crossorigin: string }
) => {
  if (!document.head.querySelector(`link[href="${href}"]`)) {
    const sheet = document.createElement('link')
    sheet.setAttribute('href', href)
    sheet.setAttribute('rel', 'stylesheet')
    if (options) {
      sheet.setAttribute('integrity', options.integrity)
      sheet.setAttribute('crossorigin', options.crossorigin)
    }
    document.head.appendChild(sheet)
  }
}

export const title = (value: string) => {
  const createTitle = () => {
    const element = document.createElement('head')
    document.head.appendChild(element)

    return element
  }

  const existingTitleElement = document.head.querySelector('title')
  const titleElement = existingTitleElement
    ? existingTitleElement
    : createTitle()
  titleElement.innerText = value
}
